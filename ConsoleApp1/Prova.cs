﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Prova
    {
       static void Main()
        {
         
            Thread thread1 = new Thread(new Threads("document1.txt").ReadTextFile);
            thread1.Start();
        
            Thread thread2 = new Thread(new Threads("document2.txt").ReadTextFile);
            thread2.Start();
  
            Thread thread3 = new Thread(new Threads("document3.txt").ReadTextFile);
            thread3.Start();

            Console.ReadKey();
        }
    }

}
