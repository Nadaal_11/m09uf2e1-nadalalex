﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Threads
    {
        private string fileName;
        public Threads(string fileName)
        {
            this.fileName = fileName;
        }
        public void ReadTextFile()
        {
            int lineCount = 0;
            using (StreamReader reader = new StreamReader( "../../Fitxers/"+ fileName))
            {
                while (reader.ReadLine() != null)
                {
                    lineCount++;
                    //Console.WriteLine(reader.ReadLine()); -------->Per mostrar les lineas
                }
            }
            Console.WriteLine("File '{0}' has {1} lines.", fileName, lineCount);
        }
    }
}
